include Formule ;;
include True ;;
include Ensemble ;;
include Tree ;;
include Valid ;;

let formule = Or(Neg(Var("a")), Var("a")) ;;

let caracterize formule = 
  match formule with
  | f when is_valid f -> "valid"
  | f when is_satisfiable f -> "satisfiable"
  | f -> "insatisfiable"
;;

print_endline (caracterize formule) ;;

let insat = And(Neg(Var("b")), Var("b")) ;;

print_endline (caracterize insat) ;;