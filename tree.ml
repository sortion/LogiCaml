type 'a tree_t = Leaf | Node of 'a * 'a tree_t * 'a tree_t ;;

let rec append_all value nested_lists = 
  match nested_lists with
  | [] -> []
  | list::tail -> [value::list]@(append_all value tail)
;;

let rec branches tree = 
  match tree with
  | Leaf -> [[];[]]
  | Node (value, left, right) -> let branches = (branches left)@(branches right) in (append_all value branches)
;;

let test_tree = Node(3, Node(5, Node(3, Node (4, Leaf, Leaf), Node(5, Leaf, Leaf)), Node(5, Leaf, Leaf)), Node(3, Leaf, Leaf)) ;;

