(** Ensemblist operations *)
let rec mem x l = 
  match l with
  | [] -> false
  | e::tail -> e=x || mem x tail
;;

let rec inter l1 l2 = 
  match l1 with
  | [] -> []
  | e::tail when mem e l2 -> e::(inter tail l2)
  | _::tail -> inter tail l2
;;

let rec union l1 l2 =
  match l1 with
  | [] -> l2
  | e::tail when mem e l2 -> union tail l2
  | e::tail -> e::union tail l2
;;