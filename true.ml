(**Verify if a formule is true*)
include Formule ;;

let rec get_interpretation variable interpretation = 
  match interpretation with
  | [] -> failwith "No interpretation for this variable"
  | (p, v)::r when p = variable -> v
  | e::r -> get_interpretation variable r 
;;

let rec is_true formule interpretation = 
  match formule with
  | Var s -> get_interpretation s interpretation
  | Neg e -> not (is_true e interpretation)
  | Or (e1, e2) -> is_true e1 interpretation || is_true e2 interpretation
  | And (e1, e2) -> is_true e1 interpretation && is_true e2 interpretation
  | Implies (e1, e2) -> if (is_true e1 interpretation && not (is_true e2 interpretation)) then false else true 
  | Equivalent (e1, e2) -> is_true e1 interpretation = is_true e2 interpretation 
;;

