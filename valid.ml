(**Verify if a formule is valid*)
include Ensemble ;;
include Combinations ;;
include Formule ;;
include True ;;

let get_all_interpretations signature =
  let all_interpretations = combinations [true; false] (List.length signature) in
  let rec match_value_with_variable signature interpretations = 
    match signature, interpretations with
    | ([], []) -> []
    | ([], i) -> failwith "Not the same number of variables and values. Not enough variables"
    | (s, []) -> failwith "Not the same number of variables and values. Not enough values"
    | (variable::other_variables, value::other_values) -> (variable, value)::(match_value_with_variable other_variables other_values)
  in
  let rec match_value_with_variable_all signature all_interpretations = 
    match all_interpretations with
    | [] -> []
    | i::tail -> (match_value_with_variable signature i)::(match_value_with_variable_all signature tail)
  in
  match_value_with_variable_all signature all_interpretations
;;

let is_valid formule = 
  let signature = get_signature formule in 
  let all_interpretations = get_all_interpretations signature in
  let rec check_all_interpretations formule interpretations = 
    match interpretations with
    | [] -> true 
    | i::r -> is_true formule i && check_all_interpretations formule r 
  in
  check_all_interpretations formule all_interpretations 
;;

let is_satisfiable formule = 
  let signature = get_signature formule in
  let all_interpretations = get_all_interpretations signature in
  let rec verify_once formule interpretations = 
    match interpretations with
    | [] -> false
    | i::tail -> is_true formule i || verify_once formule tail
  in
  verify_once formule all_interpretations
;;