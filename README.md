# LogiCaml

Classical logic for OCaml. 

<img src="./img/logo_LogiCaml.svg" alt="logo" width="200"/>

## Installation

```bash
git clone https://framagit.org/UncleSamulus/LogiCaml.git
cd LogiCaml
```

## Usage

Firstly compile the modules (needs ocamlopt)

```bash
make
```

Then in a `.ml` file:

```ocaml
include Formule ;;
include True ;;
let tautology = Or(Neg(Var("p")), Var("p")) ;;
let interpretation = [("p", true)] ;;
is_true tautology interpretation ;;
```

## Support

Questions are welcome in the issue tracker associated with this repository.

<!-- ## Roadmap -->

## Contributing

Pull requests are welcome !

## Authors and acknowledgment

Special thanks for my logic professor at Université d'Évry val d'Essonne for teaching me classical logic during my second year of Computer Science bachelor degree ;-).

## License

This project is free/libre opensource software. It is licensed under the GNU General Public License 3.0 or later. *Confer* to [./LICENSE](./LICENSE) for more details.

## Project status

This project is under development.